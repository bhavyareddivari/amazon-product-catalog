# Amazon-product-catalog

The purpose of the project is to produce and deploy a web application that allows users to browse the Amazon product catalog.
User can access this web application and see a list of products, including product images and product names.
The user can interact with a list to go to the product page on www.amazon.com.

Add in index.js:
awsId :"***********",
  awsSecret :"***************",
  awsTag : "*********"

to access the application.

